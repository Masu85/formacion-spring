package com.masu.springcloud.limitsmicroserivce;

import com.masu.springcloud.limitsmicroserivce.bean.LimitsCofiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LimitsConfigurationController
{
    @Autowired
    private Configuration configuration;

    @GetMapping("/limits")
    public LimitsCofiguration retrieveLimitsFromConfigurations()
    {
        return new LimitsCofiguration(configuration.getMaximum(), configuration.getMinimum());
    }
}
