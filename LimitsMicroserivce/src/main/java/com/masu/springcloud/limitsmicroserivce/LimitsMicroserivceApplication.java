package com.masu.springcloud.limitsmicroserivce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LimitsMicroserivceApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(LimitsMicroserivceApplication.class, args);
    }
}
