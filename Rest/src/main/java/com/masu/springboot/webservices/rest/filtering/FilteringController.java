package com.masu.springboot.webservices.rest.filtering;

import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class FilteringController
{
    @GetMapping("/filtering")
    public MappingJacksonValue retrieveSomeBean()
    {
        Set<String> filterFields = new HashSet<>();
        filterFields.add("field1");

        List beanList = Collections.singletonList(new SomeBean("value1", "value2", "value3"));

        return  filterBean(beanList, filterFields);
    }

    @GetMapping("/filtering-list")
    public MappingJacksonValue retrieveListSomeBean()
    {
        Set<String> filterFields = new HashSet<>();
        filterFields.add("field2");

        List beanList = Arrays.asList(new SomeBean("value1", "value2", "value3"),
                                      new SomeBean("value4", "value5", "value6"));

        return  filterBean(beanList, filterFields);
    }

    private MappingJacksonValue filterBean(List beanList, Set<String> filterFields)
    {
        SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept(filterFields);

        MappingJacksonValue mapping = new MappingJacksonValue(beanList);
        mapping.setFilters(new SimpleFilterProvider().addFilter("SomeBeanFilter", filter));

        return mapping;
    }
}
