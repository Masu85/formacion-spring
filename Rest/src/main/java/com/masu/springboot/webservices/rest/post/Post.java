package com.masu.springboot.webservices.rest.post;


public class Post
{
    private Integer id;
    private long    userIdCreator;
    private String  title;
    private String  content;

    public Post(Integer id, long userIdCreator, String title, String content)
    {
        this.id = id;
        this.userIdCreator = userIdCreator;
        this.title = title;
        this.content = content;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public long getUserIdCreator()
    {
        return userIdCreator;
    }

    public void setUserIdCreator(long userIdCreator)
    {
        this.userIdCreator = userIdCreator;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    @Override
    public String toString()
    {
        return "Post{id=" + id + ", userIdCreator=" + userIdCreator + ", title='" + title + '\'' + ", content='" + content + '\'' + '}';
    }
}
