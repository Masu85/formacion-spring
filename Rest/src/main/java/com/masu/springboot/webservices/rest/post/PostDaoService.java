package com.masu.springboot.webservices.rest.post;

import com.masu.springboot.webservices.rest.user.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PostDaoService
{
    private static List<Post> posts      = new ArrayList<>();
    private static int        postsCount = 3;

    static {
        posts.add(new Post(1, 1, "PrimerPost", "Hola Mundo"));
        posts.add(new Post(2, 1, "SegundoPost", "Adiós Mundo"));
        posts.add(new Post(3, 2, "ProbandoPosts", "ola q ase"));
    }

    public List<Post> findAll()
    {
        return posts;
    }

    public Post findPost(int idPost)
    {
        for(Post post : posts)
        {
            if(post.getId() == idPost)
            {
                return post;
            }
        }

        return null;
    }

    public List findUserPosts(int idUser)
    {
        List userPosts = new ArrayList();

        for(Post post : posts)
        {
            if(post.getUserIdCreator() == idUser)
            {
                userPosts.add(post);
            }
        }

        return userPosts.size() > 0 ? userPosts : null;
    }

    public Post findUserSpecificPost(int idUser, int idPost)
    {
        for(Post post : posts)
        {
            if(post.getUserIdCreator() == idUser && post.getId() == idPost)
            {
                return post;
            }
        }

        return null;
    }

    public Post save(Post post)
    {
        if(post.getId() == null)
        {
            post.setId(++postsCount);
        }

        posts.add(post);

        return post;
    }
}
