package com.masu.springboot.webservices.rest.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
public class PostResource
{
    @Autowired
    private PostDaoService postDaoService;

    @GetMapping("/posts")
    public List<Post> getAllPosts()
    {
        return postDaoService.findAll();
    }

    @GetMapping("/users/{userId}/posts")
    public List<Post> getAllUserPosts(@PathVariable int userId)
    {
        return postDaoService.findUserPosts(userId);
    }

    @GetMapping("/users/{userId}/posts/{postId}")
    public Post getUserSpecificPost(@PathVariable int userId,  @PathVariable int postId)
    {
        return postDaoService.findUserSpecificPost(userId, postId);
    }

    @PostMapping("/users/{userId}/posts")
    public ResponseEntity createPost(@PathVariable int userId, @RequestBody Post post)
    {
        post.setUserIdCreator(userId);
        Post postCreated = postDaoService.save(post);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{postId}").buildAndExpand(postCreated.getId()).toUri();

        return ResponseEntity.created(location).build();
    }
}
