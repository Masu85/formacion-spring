package com.masu.springboot.webservices.rest.postJPA;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.masu.springboot.webservices.rest.user.User;

import javax.persistence.*;

@Entity
public class PostJPA
{
    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private User    user;

    private String  description;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    @Override
    public String toString()
    {
        return "postJPA{id=" + id + ", description='" + description + '\'' + '}';
    }
}
