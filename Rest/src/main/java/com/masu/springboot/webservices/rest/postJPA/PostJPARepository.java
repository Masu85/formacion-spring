package com.masu.springboot.webservices.rest.postJPA;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostJPARepository extends JpaRepository<PostJPA, Integer>
{
}
