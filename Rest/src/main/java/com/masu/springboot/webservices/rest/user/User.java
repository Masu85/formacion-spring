package com.masu.springboot.webservices.rest.user;

import com.masu.springboot.webservices.rest.postJPA.PostJPA;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@ApiModel(description = "All detalis about the users. S")
@Entity
public class User
{
    @Id
    @GeneratedValue
    private Integer id;

    @Size(min = 2, message = "El nombre requiere un mínimo de 2 carácteres")
    @ApiModelProperty(notes = "Name should have at least 2 characters")
    private String  name;

    @Past(message = "La fecha de nacimiento no puede ser superior a la fecha actual")
    @ApiModelProperty(notes = "Birth date should be in the past")
    private Date    birthDate;

    @OneToMany(mappedBy = "user")
    private List<PostJPA> posts;

    public User(){}

    public User(Integer id, String name, Date birthDate)
    {
        super();
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Date getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(Date birthDate)
    {
        this.birthDate = birthDate;
    }

    public List<PostJPA> getPosts()
    {
        return posts;
    }

    public void setPosts(List<PostJPA> posts)
    {
        this.posts = posts;
    }

    @Override
    public String toString()
    {
        return "User{id=" + id + ", name='" + name + '\'' + ", birthDate=" + birthDate + '}';
    }
}
