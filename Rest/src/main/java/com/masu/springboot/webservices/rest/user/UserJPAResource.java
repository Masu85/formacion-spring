package com.masu.springboot.webservices.rest.user;

import com.masu.springboot.webservices.rest.postJPA.PostJPA;
import com.masu.springboot.webservices.rest.postJPA.PostJPARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class UserJPAResource
{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostJPARepository postJPARepository;

    @GetMapping("/jpa/users")
    public List<User> retrieveAllUsers()
    {
        return userRepository.findAll();
    }

    @GetMapping("/jpa/users/{id}/posts")
    public List<PostJPA> retrieveAllUserPosts(@PathVariable int id)
    {
        Optional<User> optionalUser = userRepository.findById(id);

        //if(!optionalUser.isPresent()) throw new UserNotFoundException("Id " + id + " not Found");

        return optionalUser.map(p -> p.getPosts()).orElseThrow(() -> new UserNotFoundException("Id " + id + " not Found"));
    }

    @GetMapping("/jpa/users/{id}")
    public Resource<User> retrieveUser(@PathVariable int id)
    {
        Optional<User> optionalUser = userRepository.findById(id);

        if(!optionalUser.isPresent()) throw new UserNotFoundException("Id " + id + " not Found");

        Resource<User> resource = new Resource<>(optionalUser.get());
        ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).retrieveAllUsers());

        resource.add(linkTo.withRel("all-users"));

        return resource;
    }

    @PostMapping("/jpa/users")
    public ResponseEntity createUser(@Valid @RequestBody User user)
    {
        User savedUser = userRepository.save(user);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedUser.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PostMapping("/jpa/users/{id}/posts")
    public ResponseEntity createPost(@PathVariable int id, @RequestBody PostJPA post)
    {
        Optional<User> optionalUser = userRepository.findById(id);
        if(!optionalUser.isPresent()) throw new UserNotFoundException("Id " + id + " not Found");


        post.setUser(optionalUser.get());
        PostJPA savedPost = postJPARepository.save(post);


        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedPost.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @DeleteMapping("/jpa/users/{id}")
    public void deleteUser(@PathVariable int id)
    {
        userRepository.deleteById(id);
    }

}
